// import packages
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require('request-promise');
const cred = require('../airTable.json');
const secureCompare = require('secure-compare');
const serviceAccount = require("../serviceAccountKey.json");
const Airtable = require('airtable');

// set admin credential
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: ""
});

const sleep = ((ms:any) => {return new Promise(resolve => setTimeout(resolve, ms))});

exports.updateEveryday = functions.https.onRequest(async (req:any, res:any) => {
	const key = req.query.key;

	// Exit if the keys don't match.
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key,
		    'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' +
		    'cron.key environment variable.');
		return null;
	}

	// get string format of yesterday 
	const yesterday = new Date();
	yesterday.setDate(yesterday.getDate()-7);
	const yesterdayString = yesterday.toISOString().replace(/T.+/,"").replace(/\-/g,'');
	console.log(yesterdayString);


	return await request(
		{
		  uri: "https://data.cityofnewyork.us/id/6bgk-3dad.json?issue_date=" + yesterdayString,
		  method: "GET"
		}, async(err:any, result:any, body:any) => {
		if(err){
			console.log(err);
			res.status(403).send('Request Error');
			return null;
		} else {

			// get array from JSON string
			const array = JSON.parse(body);

			// create new base for AirTable
			const base = new Airtable({apiKey: cred.AIRTABLE_API_KEY}).base('appAf7RWbd9EFZgzD');
			
			console.log(array.length);
			

			for (const record of array){
				const record1 = {
					ecb_violation_number: record.ecb_violation_number,
					bin: record.bin,
					isn_dob_bis_extract : record.isn_dob_bis_extract,
					infraction_code1 : record.infraction_code1,
					section_law_description1 : record.section_law_description1,
					respondent_name : record.respondent_name,
					respondent_house_number : record.respondent_house_number,
					respondent_street : record.respondent_street,
					respondent_city : record.respondent_city,
					respondent_zip : record.respondent_zip,
					block : record.block,
					lot :record.lot,
					violation_type : record.violation_type,
					violation_description : record.violation_description,
					aggravated_level : record.aggravated_level,
					issue_date : record.issue_date.slice(4,6) + "-" + record.issue_date.slice(6,8) + "-" + record.issue_date.slice(0,4),
					served_date : record.served_date.slice(4,6) + "-" + record.served_date.slice(6,8) + "-" + record.served_date.slice(0,4),
					hearing_status : record.hearing_status,
					hearing_time :record.hearing_time,
					hearing_date : record.hearing_date.slice(4,6) + "-" + record.hearing_date.slice(6,8) + "-" + record.hearing_date.slice(0,4),
					certification_status : record.certification_status
				}
				
				base('DOB ECB Violations').create(record1, function(err1:any, rec:any) {
				  if (err1) {
				    console.error(err1);
				    res.status(403).send('Error for adding new record to AirTable');
				  }
				});

				await sleep(200);
			}
			res.status(200).send('Success');
			return;
		}
	});

});
